using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    interface ILogable
    {
        string GetStringRepresentation();
    }
}
