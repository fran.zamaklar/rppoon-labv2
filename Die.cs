using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    class Die
    {
        private int numberOfSides;
        //private Random randomGenerator; Prvi i drugi zadatak
        private RandomGenerator randomGenerator; 
        /* Prvi zadatak
        public Die(int numberSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }*/
        
        /*Drugi zadatak 
         public Die(int numberOfSides, Random randomGen)
         {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGen;
        }*/
        
        public Die(int numberSides)
        {
            this.numberOfSides = numberSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int GetNumberOfSides()
        {
            return numberOfSides;
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
