using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultsForEachRoll;
        private ILogger logger;

        /* 2 zadatak
          public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultsForEachRoll = new List<int>();
        }
        */
            
        public DiceRoller(ILogger logger)
        {
            this.dice = new List<Die>();
            this.resultsForEachRoll = new List<int>();
            this.logger = logger ;
        }


        public void InsertDice(Die die)
        {
            dice.Add(die);
        }

        public void RollDices()
        {
            this.resultsForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultsForEachRoll.Add(die.Roll());
            }
        }

        public IList<int> GetRollingResults()
        {
            return new List<int>(this.resultsForEachRoll);
           
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

        public string GetStringRepresentation()
        {
            return string.Join(", ", GetRollingResults());
        }

    }
}
