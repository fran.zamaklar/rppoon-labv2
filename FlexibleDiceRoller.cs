using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    class FlexibleDiceRoller :  IFlexibleDiceRoller, ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public void RemoveDice(int numberOfSides)
        {
            
         this.dice.RemoveAll(dice => dice.GetNumberOfSides() == numberOfSides);
               
           
        }
        
        public string GetStringRepresentation()
        {
            List<int> SidesNumber = new List<int>();
            for(int i = 0; i < dice.Count; i++)
            {
                SidesNumber.Add(this.dice[i].GetNumberOfSides());
            }
            return string.Join(", " , SidesNumber);
        }
    }
}
