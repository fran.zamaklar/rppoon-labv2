using System;

namespace LAB2
{
    class Entry
    {
        static void Main(string[] args)
        {
            //DiceRoller diceroller = new DiceRoller(); Prvi, drugi, treci zadatak
            //Random randomgen = new Random(); Drugi zadatak  
            FileLogger fileLogger = new FileLogger("File name");
            DiceRoller diceroller = new DiceRoller(fileLogger);
            ConsoleLogger consoleLogger = new ConsoleLogger();
            for(int i = 0; i < 20; i++)
            {
                Die die = new Die(6);
                //Die die = new Die(6, randomgen); Drugi zadatak
                diceroller.InsertDice(die);
            }

            diceroller.RollDices();

            //string results = string.Join(", ", diceroller.GetRollingResults()); Prvi, drugi, treci i cetvrti zadatak

            /* Cetvrti zadatak
            fileLogger.Log(results);
            fileLogger.Close();
            consoleLogger.Log(results);
            */

            fileLogger.Log(diceroller);
            //fileLogger.Close(); Peti zadatak
            consoleLogger.Log(diceroller);

            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();

            for(int i = 0; i < 10; i++)
            {
                int randomnumber = RandomGenerator.GetInstance().NextInt(1,5);
                Die die = new Die(randomnumber);
                flexibleDiceRoller.InsertDie(die);
            }

            flexibleDiceRoller.RemoveDice(4);
            flexibleDiceRoller.RollAllDice();

            fileLogger.Log(flexibleDiceRoller);
            fileLogger.Close();
            consoleLogger.Log(flexibleDiceRoller);

            //Console.WriteLine(results); Prvi, drugi i treci zadatak
            //Console.WriteLine(diceroller.DiceCount); Prvi zadatak
        }
        
    }
}
