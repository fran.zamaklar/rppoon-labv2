using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    interface IFlexibleDiceRoller : IDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
