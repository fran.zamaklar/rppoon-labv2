using System;
using System.Collections.Generic;
using System.Text;

namespace LAB2
{
    class ConsoleLogger : ILogger
    {
        public ConsoleLogger() { }
        /* Cetvrti zadatak
        public void Log(string message)
        {
            Console.WriteLine(message);
        }*/
        public void Log(ILogable data) {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
