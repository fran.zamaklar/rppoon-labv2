using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LAB2
{
    class FileLogger : ILogger
    {
        private StreamWriter file;
        public FileLogger(string path)
        {
            file = new StreamWriter(path);
        }
        /* Cetvrti zadatak
        public void Log(string message)
        {
            file.WriteLine(message);
        }*/
        public void Log(ILogable data)
        {
            file.WriteLine(data.GetStringRepresentation());
        }
        public void Close()
        {
            file.Close();
        }
    }
}
